import React from 'react';
import { Image } from 'react-native';
import shorthash from 'shorthash';
import { FileSystem } from 'expo';

export default class SmartImage extends React.Component {
  state = {
    source: {
      isStatic: true
    }
  };

  componentDidMount = async () => {
    const { uri } = this.props;
    const name = shorthash.unique(uri);
    const path = `${FileSystem.cacheDirectory}${name}`;
    const image = await FileSystem.getInfoAsync(path);

    if (image.exists) {
      this.setState({
        source: {
          ...this.state.source,
          uri: image.uri
        }
      });
      return;
    }

    const newImage = await FileSystem.downloadAsync(uri, path);
    this.setState({
      source: {
        ...this.state.source,
        uri: newImage.uri
      }
    });
  };

  render() {
    return <Image style={this.props.style} source={this.state.source} />;
  }
}



// import React from 'react';
// import { Image } from 'react-native';
// import shorthash from 'shorthash';
// import { FileSystem } from 'expo';

// export default class SmartImage extends React.Component {
//   state = {
//     source: null,
//   };

//   componentDidMount = async () => {
//     console.log(this.props.style);
//     const { uri } = this.props;
//     const name = shorthash.unique(uri);
//     const path = `${FileSystem.cacheDirectory}${name}`;
//     const image = await FileSystem.getInfoAsync(path);

//     if (image.exists) {
//       console.log('read image from cache');
//       console.log("CACHE IMAGE: "+image.uri);
//       this.setState({
//         source: {
//           uri: image.uri,
//         },
//       });
//       return;
//     }

//     console.log('downloading image to cache');
//     const newImage = await FileSystem.downloadAsync(uri, path);
//     console.log("NEW IMAGE: "+newImage.uri);
//     this.setState({
//       source: {
//         uri: newImage.uri,
//       },
//     });
//   };

//   render() {
//       console.log("SOURCE: "+JSON.stringify(this.state.source));
//     return <Image height="200" width="200" style={this.props.style} source={this.state.source} />;
//   }
// }
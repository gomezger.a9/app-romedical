import * as Expo from 'expo'
import { Asset, AppLoading, Font } from 'expo';
Expo.Asset;
Asset;
import React from 'react';
import { Text } from 'react-native';
import LoginScreen from './screens/LoginScreen';
import MainScreen from './screens/MainScreen';
import SplashScreen from './screens/SplashScreen';
import ForgetPasswordScreen from './screens/ForgetPasswordScreen';
import RegisterScreen from './screens/RegisterScreen';
import { YellowBox } from 'react-native';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Provider, connect } from "react-redux";
import { Provider as ProviderAD } from '@ant-design/react-native';
import { createStore } from 'redux';
import { appReducer } from './reducers/AppReducer';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { PersistGate } from 'redux-persist/integration/react';

import NavigationService from './NavigationService';


const persistConfig = {
  key: 'root',
  storage,
}
YellowBox.ignoreWarnings(['Remote debugger']);
const persistedReducer = persistReducer(persistConfig, appReducer)
const store = createStore(persistedReducer);
const persistor = persistStore(store);
// const store = createStore(appReducer);

export default class App extends React.Component {
  state = {
    theme: null,
    currentTheme: null,
    isReady: false,
  };
  changeTheme = (theme, currentTheme) => {
    this.setState({ theme, currentTheme });
  };
  async componentDidMount() {
    console.disableYellowBox = true;
    await Font.loadAsync(
      'antoutline',
      // eslint-disable-next-line
      require('@ant-design/icons-react-native/fonts/antoutline.ttf')
    );

    await Font.loadAsync(
      'antfill',
      // eslint-disable-next-line
      require('@ant-design/icons-react-native/fonts/antfill.ttf')
    );
    // eslint-disable-next-line
    this.setState({ isReady: true });
  }
  
  render() {
    const { theme, currentTheme, isReady } = this.state;
    if (!isReady) {
      return <AppLoading />;
    }

    return (
      <Provider store={store}>
        <ProviderAD theme={theme}>
            <PersistGate persistor={persistor}>
              <AppStackNavigator
                onNavigationStateChange={this.handleNavigationChange}
                screenProps={{ changeTheme: this.changeTheme, currentTheme }}
                ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}>
              </AppStackNavigator>
            </PersistGate>
        </ProviderAD>
      </Provider>
    );
  }

  handleNavigationChange = () =>{

  }
}

const AppStackNavigator = createAppContainer(createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
  },
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
      gesturesEnabled: false,
      cardStack: {
        gesturesEnabled: false,
      },
    },
  },
  MainScreen: {
    screen: MainScreen,
    navigationOptions: {
      gesturesEnabled: false,
      headerLeft: null,
      cardStack: {
        // gesturesEnabled: false,
      },
    },
  },
  ForgetPasswordScreen: {
    screen: ForgetPasswordScreen,
    navigationOptions: {
      cardStack: {
        gesturesEnabled: false,
      },
    },
  },
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      cardStack: {
        gesturesEnabled: false,
      },
    },
  },
},
{
  cardStyle: { backgroundColor: '#FFFFFF' }
}
));

// Este archivo actua como index.js. El comando de abajo funciona como solucionador del problema de pbxcp 

//react-native bundle --entry-file App.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ios


{/* <key>NSAppTransportSecurity</key>
	<!--See http://ste.vn/2015/06/10/configuring-app-transport-security-ios-9-osx-10-11/ -->
	<dict>
		<key>NSExceptionDomains</key>
		<dict>
			<key>localhost</key>
			<dict>
				<key>NSExceptionAllowsInsecureHTTPLoads</key>
				<true/>
			</dict>
		</dict>
	</dict>
</dict> */}
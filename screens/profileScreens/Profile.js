import React from 'react';
import { Image, ScrollView, StyleSheet, View, Text, RefreshControl, BackHandler, Alert } from 'react-native';
import { Result, Button } from '@ant-design/react-native';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';

import NavigationService from '../../NavigationService';

class Profile extends React.Component {
  
    constructor(props) {
        super(props);
        this.state = {
            fetchUserData : false,
            isLoading     : false,
            refreshing    : false
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }  

    onButtonPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        // then navigate
        navigate('NewScreen');
    }

    handleBackButton = () => {
        BackHandler.exitApp()  
        return true;
    } 
    render() {
    if(!this.props.userIsLogged){
        return(
            <View style={{paddingRight: 5, paddingLeft: 5, marginTop:15}}>
                <Text style={{ textAlign: 'center' ,marginBottom: 15, color: "grey"}}>Para acceder al perfil deberá registrarse e identificarse.</Text>
                <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goLogIn}>Identificarse</Button>
            </View>
        );
    }else{
        var color = "black";
        var saldo = this.props.saldoTarjeta;
        if(saldo > 0){
            color = "red";
            saldo = '-'+saldo;
        }
        return (
        <ScrollView 
            style={{ backgroundColor: '#F5F5F9', flex: 1 }}
            refreshControl = {
                        <RefreshControl 
                            refreshing = {this.state.refreshing}
                            onRefresh = {this._onRefresh}
                        />
                    }>
            <Result
                img={<Image
                    source={ this.props.gender == "MASC" ? require('../../assets/images/profile_man.png') : require('../../assets/images/profile_woman.png')}
                    style={{ width: 100, height: 100 }}
                    />}
                title={<Text style={{fontFamily: 'Gotham-Light', fontSize: 20}}>{"Hola "+this.props.name+"!"}</Text>}
            />
            <View style={{ alignItems: 'center', paddingTop: 25, paddingBottom: 25, flex: 1 }}>
                <Text style={{fontSize: 25, fontFamily: "Gotham-Light"}}>Saldo Cuenta Corriente</Text>
                <Text style={{fontSize: 25, color: color, fontFamily: "Gotham-Medium"}}>AR$ {saldo}</Text>
            </View>
            <View style={styles.ViewTextContainer}>
                <Text style={styles.textContainer}>    
                    <Text style={styles.title}>Nombre: </Text>
                    <Text style= {styles.text}>{this.props.name}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Género: </Text>
                    <Text style={styles.text}>{this.props.gender}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Email: </Text>
                    <Text style={styles.text}>{this.props.email}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Teléfono: </Text>
                    <Text style={styles.text}>{this.props.phone}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Fecha nacimiento: </Text>
                    <Text style={styles.text}>{this.props.birthdate}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Dirección: </Text>
                    <Text style={styles.text}>{this.props.address}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>CP: </Text>
                    <Text style={styles.text}>{this.props.codigoPostal}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Localidad: </Text>
                    <Text style={styles.text}>{this.props.localidad}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>DNI: </Text>
                    <Text style={styles.text}>{this.props.doc}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Profesión: </Text>
                    <Text style={styles.text}>{this.props.profesion}</Text>
                </Text>

                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Contacto de emergencia médica: </Text>
                    <Text style={styles.text}>{this.props.emergenciaMedica}</Text>
                </Text>
                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Teléfono de emergencia médica: </Text>
                    <Text style={styles.text}>{this.props.emergenciaMedicaTel}</Text>
                </Text>
                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Número de afiliado: </Text>
                    <Text style={styles.text}>{this.props.obraSocialNAfiliado}</Text>
                </Text>
                <Text style={styles.textContainer}>
                    <Text style={styles.title}>Obra social: </Text>
                    <Text style={styles.text}>{this.props.obraSocialPlan}</Text>
                </Text>
            </View>
            <View style={{padding: 5}}>
                <Button type="primary" style={styles.button} onPress={() => { this.props.navigation.navigate("Edit"); }}>Editar datos</Button>
                <Button type="warning" onPress={this.logOut}>Log out</Button>
            </View>
        </ScrollView>
        );
    }
  }

  logOutConfirm = () =>{
    this.props.dispatch({type: "LOG_OUT"});
    NavigationService.navigate("LoginScreen");
   }

   goLogIn = () => {
        NavigationService.navigate("MainScreen");
        // this.props.screenProps.rootNavigation.navigate("LoginScreen");
    }

    logOut = () => {
        Alert.alert(
            'Log Out',
            'Quiere desloguearse?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, 
            {
                text: 'Log out',
                onPress: () => this.logOutConfirm()
            }, 
        ], 
            {
            cancelable: false
            }
         )
         return true;
       } 

    _onRefresh = () => {
        this.setState({ 
            refreshing     : true,
            isLoading      : true,
            fetchUserData  : true
        });
        TimerMixin.setTimeout(() => {
        this.fetchData()
            .then(() => {
            this.setState({ 
                refreshing     : false,
                fetchUserData  : false,
                isLoading      : false
            })
        })
        .catch((err) => {
            console.log("Fallo", err)
        })
        }, 2500)
    }

    async fetchData() {
        return fetch('http://romedicalsistema.com/api/getUserData.php', {
            method: 'post',
            body: 'email='+this.props.email+'&token='+this.props.token,
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {
            if(res.status == 200){
                this.props.dispatch({type: 'LOAD_USER_DATA', data: res.data})
            }
            this.setState({fetchUserData: false});            
          }).catch((error) => {
                console.error(error);
                this.setState({fetchUserData: true});                
         });
    }
}

const mapStateToprops = state => {
    return{
      userIsLogged: state.userIsLogged,
      clientId: state.clientId,
      email: state.email,
      token: state.token,
      phone : state.phone,
      birthdate : state.birthdate,
      address: state.address,
      codigoPostal: state.codigoPostal,
      localidad: state.localidad,
      name: state.name,
      doc: state.doc,
      profesion: state.profesion,
      emergenciaMedicaTel: state.emergenciaMedicaTel,
      emergenciaMedica: state.emergenciaMedica,
      obraSocialNAfiliado: state.obraSocialNAfiliado,
      obraSocialPlan: state.obraSocialPlan,
      obraSocial: state.obraSocial,
      hijos: state.hijos,
      gender: state.gender,
      saldoTarjeta: state.saldoTarjeta,
    }
}

const styles = StyleSheet.create({
    button: {
        marginBottom: 5,
        backgroundColor: "#5cc0ee",
        borderColor: "#5cc0ee"
    },
    textContainer: {
        textAlign: 'auto'
    },
    ViewTextContainer: {
        backgroundColor: "white", 
        paddingRight: 15,
        paddingTop:10,
        paddingBottom: 10,
        paddingLeft: 35,
        marginTop: 45,
        marginBottom:25, 
        
    },
    title:{
        fontFamily: 'Gotham-Medium',
        fontWeight: 'bold',
        fontSize: 20,
        color:'#65BBE9',
    },
    text: {
        fontFamily: 'Gotham-Light',
        fontSize: 20
        
    }
});

export default connect(mapStateToprops)(Profile);
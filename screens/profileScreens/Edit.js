import React from 'react';
import {View, ScrollView, Text, StyleSheet, ActivityIndicator, Keyboard, BackHandler, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { Button, InputItem, Picker, List } from '@ant-design/react-native';
import TimerMixin from 'react-timer-mixin';
import NavigationService from '../../NavigationService';

class Edit extends React.Component{
    constructor(props){
        super(props);
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    state = {
        clientId: this.props.clientId,
        userIsLogged: this.props.userIsLogged,
        phone : this.props.phone,
        birthdate : this.props.birthdate,
        address: this.props.address,
        localidad: this.props.localidad,
        codigoPostal: this.props.codigoPostal,
        name: this.props.name,
        doc: this.props.doc,
        profesion: this.props.profesion,
        emergenciaMedicaTel: this.props.emergenciaMedicaTel,
        emergenciaMedica: this.props.emergenciaMedica,
        obraSocialNAfiliado: this.props.obraSocialNAfiliado,
        obraSocialPlan: this.props.obraSocialPlan,
        obraSocial: this.props.obraSocial,
        hijos: this.props.hijos,
        gender: this.props.gender,
        token: this.props.token,
        isLoading: false,
        message: ""
    }
    
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render(){
        console.log("RERENDER");
        if(!this.props.userIsLogged){
            return(
                <View style={{paddingRight: 5, paddingLeft: 5, marginTop: 15}}>
                    <Text style={{ textAlign: 'center' ,marginBottom: 15, color: "grey"}}>Para acceder al perfil deberá registrarse e identificarse.</Text>
                    <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goLogIn}>Identificarse</Button>
                </View>
            );
        }else{
            return(
                <ScrollView style={{paddingRight: 5, paddingLeft: 5}}>
                <TouchableOpacity onPress={Keyboard.dismiss()}>
                    <List>
                        <InputItem
                            placeholder="Nombre"
                            defaultValue={this.state.name}
                            onChangeText={(el) => {this.state.name = el;}}
                        >
                            <Text style={{color: 'grey'}}>Nombre</Text>
                        </InputItem>
                        <InputItem
                            placeholder="Teléfono"
                            defaultValue={this.state.phone}
                            onChangeText={(el) => {this.state.phone = el;}}
                        >
                            <Text style={{color: 'grey'}}>Teléfono</Text>
                        </InputItem>
                        <InputItem
                            placeholder="Dirección"
                            defaultValue={this.state.address}
                            onChangeText={(el) => {this.state.address = el;}}
                        >
                            <Text style={{color: 'grey'}}>Dirección</Text>
                        </InputItem>
                        <InputItem
                            placeholder="Localidad"
                            defaultValue={this.state.localidad}
                            onChangeText={(el) => {this.state.localidad = el;}}
                        >
                            <Text style={{color: 'grey'}}>Localidad</Text>
                        </InputItem>
                        <InputItem
                            placeholder="Código Postal"
                            defaultValue={this.state.codigoPostal}
                            onChangeText={(el) => {this.state.codigoPostal = el;}}
                        >
                            <Text style={{color: 'grey'}}>CP</Text>
                        </InputItem>
                        <InputItem
                            placeholder="Documento"
                            defaultValue={this.state.doc}
                            onChangeText={(el) => {this.state.doc = el;}}
                        >
                            <Text style={{color: 'grey'}}>DNI</Text>
                        </InputItem>
                        <InputItem
                            placeholder="Profesión"
                            defaultValue={this.state.profesion}
                            onChangeText={(el) => {this.state.profesion = el;}}
                        >
                            <Text style={{color: 'grey'}}>Profesión</Text>
                        </InputItem>
                        <Picker
                            data={[{label: 'Masculino',value: 'MASC'},{label: 'Femenino',value: 'FEM'}]}
                            cols={1}
                            okText="Seleccionar"
                            dismissText="Cancelar"
                            extra={this.state.gender}
                            onOk={(val) => { this.setState({gender: val}) }}
                        >
                            <List.Item arrow="horizontal" style={{color: "grey", fontSize: 8}}>Género</List.Item>
                        </Picker>
                        <Picker
                            data={[{label: 'Si',value: 'SI'},{label: 'No',value: 'NO'}]}
                            cols={1}
                            okText="Seleccionar"
                            dismissText="Cancelar"
                            extra={this.state.hijos}
                            onOk={(val) => { this.setState({hijos: val})} }
                        >
                            <List.Item arrow="horizontal">Hijos</List.Item>
                        </Picker>
                        <Button type="primary" style={styles.button} onPress={this.editUserData}>Guardar</Button>
                        <ActivityIndicator style={{marginTop: 5}} size="large" color="#5cc0ee" animating={ this.state.isLoading }/>
                        <Text style={{marginTop: 5, textAlign: "center", color: 'grey'}}>{this.state.message}</Text>
                        </List>
                    </TouchableOpacity>
                </ScrollView>
            );
        }
    }
    goLogIn = () => {
        NavigationService.navigate("LoginScreen");
        this.props.screenProps.mainNavigation.navigate("LoginScreen");
    }

    createPostValues = (object) =>{
        var str = "";
        var cont = 0;
        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                if(cont ==0)
                    str+=key+"="+object[key];
                else
                    str+="&"+key+"="+object[key];
            }
            cont++;
        }
        return str;
    }

    editUserData = () => {
        // this.props.dispatch({type: "CHANGE_GENDER", payload: this.state.gender})
        Keyboard.dismiss();
        this.setState({isLoading: true});
        fetch('http://romedicalsistema.com/api/setClientData.php', {
            method: 'post',
            body: this.createPostValues(this.state),
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {
            //   console.log(res);
            this.setState({isLoading: false});
            if(res.status == 200){
                this.setMessage("Los datos se han editado correctamente", 6000);
                fetch('http://romedicalsistema.com/api/getUserData.php', {
                    method: 'post',
                    body: 'email='+this.props.email+'&token='+this.props.token,
                    headers: { 'Content-type': 'application/x-www-form-urlencoded' }
                }).then((response) => response.json())
                  .then((res) => {
                    if(res.status == 200){
                        this.props.dispatch({type: 'LOAD_USER_DATA', data: res.data})
                        this.props.navigation.navigate("Profile")
                    }
                  }).catch((error) => {
                        console.error(error);
                 }); 
            }else{
                this.setMessage("Error, no se pudieron editar los datos", 6000);
            }
          }).catch((error) => {
                this.setState({isLoading: false});
                console.error(error);
         });
    }
    
    setMessage(message, time=2000){
        this.setState({message})
        TimerMixin.setTimeout(
            () => { this.setState({message: ""}) },
            time
        );
    }
}

const styles = StyleSheet.create({
    button: {
        marginBottom: 5,
        backgroundColor: "#5cc0ee",
        borderColor: "#5cc0ee"
    }
});

const mapStateToprops = state => {
    return{
        clientId: state.clientId,
        email: state.email,
        userIsLogged: state.userIsLogged,
        phone : state.phone,
        birthdate : state.birthdate,
        address: state.address,
        localidad: state.localidad,
        codigoPostal: state.codigoPostal,
        name: state.name,
        doc: state.doc,
        profesion: state.profesion,
        emergenciaMedicaTel: state.emergenciaMedicaTel,
        emergenciaMedica: state.emergenciaMedica,
        obraSocialNAfiliado: state.obraSocialNAfiliado,
        obraSocialPlan: state.obraSocialPlan,
        obraSocial: state.obraSocial,
        hijos: state.hijos,
        gender: state.gender,
        token: state.token
    }
}

export default connect(mapStateToprops)(Edit);
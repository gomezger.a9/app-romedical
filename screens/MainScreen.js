import { Asset } from 'expo';
Asset;
import React from 'react';

import { Ionicons } from '@expo/vector-icons';

import {
  createBottomTabNavigator,
  createStackNavigator,
} from 'react-navigation';

import { TouchableWithoutFeedback, Image, View, Keyboard } from 'react-native';

import ListNews from './newsScreens/ListNews';
import News from './newsScreens/News';

import Affections from './homeScreens/Affections';
import Treatment from './homeScreens/Treatment';
import Treatments from './homeScreens/Treatments';

import Benefit from './benefitsScreens/Benefit';
import ListBenefits from './benefitsScreens/ListBenefits';
import Cupon from './benefitsScreens/Cupon';

import ListShifts from './shiftsScreens/ListShifts';
import NewShift from './shiftsScreens/NewShift';

import Profile from './profileScreens/Profile';
import Edit from './profileScreens/Edit';

import { connect } from 'react-redux';

const BenefitsNavigator = createStackNavigator({
  ListBenefits: {
    screen: ListBenefits,
    navigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
  },
  Benefit: {
      screen: Benefit,
      navigationOptions: {
          header: null,
      },
  },
  Cupon: {
    screen: Cupon,
      navigationOptions: {
          header: null,
      },
  }
},
{cardStyle: { backgroundColor: '#FFFFFF' }}
);

const NewsNavigator = createStackNavigator({
  ListNews: {
    screen: ListNews,
    navigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
  },
  News: {
      screen: News,
      navigationOptions: {
          header: null,
      },
  },
},
{cardStyle: { backgroundColor: '#FFFFFF' }}
);

const HomeNavigator = createStackNavigator({
  Main: {
    screen: Affections,
    navigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
  },
  Treatments: {
    screen: Treatments,
    navigationOptions: {
        header: null,
    },
  },
  Treatment: {
    screen: Treatment,
    navigationOptions: {
        header: null
    },
  }
},
{cardStyle: { backgroundColor: '#FFFFFF' }}
);

const ShiftsNavigator = createStackNavigator({
  ListShifts: {
    screen: ListShifts,
    navigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
  },
  NewShift: {
    screen: NewShift,
    navigationOptions: {
        header: null,
    },
  }
},
{cardStyle: { backgroundColor: '#FFFFFF' }}
);

const ProfileNavigator = createStackNavigator({
  Profile: {
    screen: Profile,
    navigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
  },
  Edit: {
    screen: Edit,
    navigationOptions: {
        header: null,
    },
  },
}
);

const mapStateToprops = state => {
  return{
    selectedMainScreenTab: state.selectedMainScreenTab,
  }
}

export default BottomTabNavigator = connect(mapStateToprops)(createBottomTabNavigator(
  {
    News: {
      screen: NewsNavigator,
      navigationOptions: {
        tabBarLabel:"Novedades",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-text" size={31} color="#5cc0ee"/>
        )
      },
    },
    Benefits: {
      screen: BenefitsNavigator,
      navigationOptions: {
        tabBarLabel:"Beneficios",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="ios-gift" size={31} color="#5cc0ee"/>
        )
      },
    },
    Home: {
      screen: HomeNavigator,
      navigationOptions: {
        tabBarLabel:"Tratamientos",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-medkit" size={31} color="#5cc0ee"/>
        )
      },
    },
    Shifts:{
      screen: ShiftsNavigator,
      navigationOptions: {
        tabBarLabel:"Turnos",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-calendar" size={31} color="#5cc0ee"/>
        )
      },
    },
    Profile: {
      screen: ProfileNavigator,
      navigationOptions: {
        header: null,
        tabBarLabel: "Perfil",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-people" size={31} color="#5cc0ee"/>
        ),
      },
    }
  },
  {
    navigationOptions: {
      headerTitle: 
      <TouchableWithoutFeedback onPress={()=>{ Keyboard.dismiss() }}>
        <View style={{flex: 1,alignItems: 'center',justifyContent: 'center',backgroundColor: 'white'}}>
          <Image style={{width:240, height: 30}} source={require("../assets/images/logoheader.png")}/>
        </View>
      </TouchableWithoutFeedback>,
    },
    tabBarOptions: { 
      activeTintColor:'#5cc0ee',
      inactiveTintColor: 'grey'
    },
  }
));

{/* <View style={{flex: 1,alignItems: 'center',
        justifyContent: 'center', 
        shadowOpacity: 0.4,
        backgroundColor: 'white'}}></View> */}
  
// export default class MainScreen {
//   render(){
//     return(
//       <BottomTabNavigator 
//         navigationOptions={{
//           headerTitle: 
//           <TouchableWithoutFeedback onPress={()=>{ Keyboard.dismiss() }}>
//             <View style={{flex: 1,alignItems: 'center',justifyContent: 'center',}}>
//               <Image style={{width:240, height: 30}} source={require("../assets/images/logoheader.png")}/>
//             </View>
//           </TouchableWithoutFeedback>
//         }}
//         tabBarOptions={{ 
//           activeTintColor:'#5cc0ee',
//           inactiveTintColor: 'grey'
//         }}
//       />
//     );
//   }
// }
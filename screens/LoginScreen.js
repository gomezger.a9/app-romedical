import React from 'react';
import { View, StyleSheet, Keyboard, Text, Image, Dimensions } from "react-native";
import { Button, InputItem } from '@ant-design/react-native';
import Toast from 'react-native-root-toast';
import axios from 'axios';
import { connect } from 'react-redux';
import { BackHandler, ActivityIndicator, TouchableWithoutFeedback } from 'react-native'
import TimerMixin from 'react-timer-mixin';
import { Permissions, Notifications, Font, Asset } from 'expo';
import NavigationService from '../NavigationService';


const screenWidth = Dimensions.get('window').width

const cacheImages = images => images.map(image => {
    if (typeof image === 'string') return Image.prefetch(image);
    return Asset.fromModule(image).downloadAsync();
});

const PUSH_ENDPOINT = 'https://your-server.com/users/push-token';

async function getToken() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    let tokenTWN = 'TWN' + await Notifications.getExpoPushTokenAsync();
    console.log(tokenTWN)
    return tokenTWN;
  }else {
    // Get the token that uniquely identifies this device
  let token = await Notifications.getExpoPushTokenAsync();
  console.log(token);
  return token;
  }

  
}

class LoginScreen extends React.Component{
    _didFocusSubscription;
    _willBlurSubscription;

    static navigationOptions = {
        headerLeft: null,
        // title: 'Services',
        // headerTintColor: "white",
        headerStyle: {
        // backgroundColor: '#e6097a',
        },
    };
    state = {
        email: "",
        password: "",
        message: "",
        isLoading: false,
        errorEmail: false,
        errorPass: false,
        fontLoaded: false
    }
    constructor(props){
        super(props);
        navigation = this.props.navigation;

        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
        BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }
    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        await Font.loadAsync({
          'Gotham-Medium': require('../assets/fonts/Gotham-Medium-Regular.ttf'),
        });
    
        this.setState({ fontLoaded: true });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        BackHandler.exitApp();
        return true;
    }

    render(){
        if(this.state.fontLoaded){
            return(
                <View style={styles.container}>
                    <TouchableWithoutFeedback onPress={ () => Keyboard.dismiss()}>
                        <View>
                            <View style={{justifyContent: 'center',alignItems: 'center', marginBottom: 50}}>
                                <Image
                                    style={{ width: 288, height: 36 }}
                                    source={require('../assets/images/logoheader.png')}
                                />
                            </View>
                            
                            <InputItem
                                clear
                                type="email"
                                error={this.state.errorUser}
                                style={{marginBottom: 15}}
                                placeholder="Email"                            
                                onChangeText={(el) => {this.state.email = el; this.setState({errorEmail: false})}}
                            />
                            <InputItem
                                clear
                                type="password"
                                error={this.state.errorPass}
                                style={{marginBottom: 15}}
                                placeholder="Contraseña"
                                secureTextEntry={true}
                                password={true}
                                // ref= {(el) => { el = this.state.password; }}
                                onChangeText={(el) => {this.state.password = el; this.setState({errorPass: false})}}
                            />
                            <Button type="primary" style={styles.button} onPress={this.checkLogin}>Entrar</Button>
                            <Button type="primary" style={styles.button} onPress={this.registerUser}>Registrarse</Button>
                            <Button type="primary" style={styles.button} onPress={this.enterDisconnected}>Invitado</Button>
                            <Text style={{fontFamily: "Gotham-Medium", marginTop: 25, textAlign: "center", color: 'grey'}} onPress={ () => this.props.navigation.navigate('ForgetPasswordScreen') }>
                                ¿Olvidó su contraseña?
                            </Text>
                            <ActivityIndicator size="large" color="#5cc0ee" animating={ this.state.isLoading }/>
                            <Text style={{marginTop: 5, textAlign: "center", color: 'grey'}}>{this.state.message}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                 </View>
            );
        }else{
            return <ActivityIndicator size="large" color="#5cc0ee" animating/>
        }
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    onBackButtonPressAndroid = () => {
        return false;
    };

    setMessage(message){
        this.setState({message})
        TimerMixin.setTimeout(
            () => { this.setState({message: ""}) },
            2000
        );
    }
    registerUser = () => {
        NavigationService.navigate("RegisterScreen");
        // navigation.navigate('RegisterScreen');
    }

    enterDisconnected = () => {
        this.setState({isLoading: true});
        this.props.dispatch({type: "CHANGE_MAINSCREEN_TAB", payload: "home"});
        navigation.navigate('MainScreen');
        this.setState({isLoading: false});
    }

    checkLogin = () => {
        
        
        Keyboard.dismiss();
        if(!this.validateEmail(this.state.email)){
            //this.setMessage("Error, formato de email inválido");
                this.setState({errorEmail:true});
                return;
        }

        getToken().then((token) =>{
            this.props.dispatch({type: 'SET_TOKEN', payload: token})
            if(token == null){
                toastError = <Toast visible> No se puede loguear </Toast>
                this.setMessage(toastError);
                this.setState({errorEmail:true, errorPass: true})
                
                
                fetch('http://romedicalsistema.com/api/getUserData.php', {
                    method: 'post',
                    body: 'email='+this.state.email+'&password='+this.state.password+'&token='+token,
                    headers: { 'Content-type': 'application/x-www-form-urlencoded' }
                }).then((response) => response.json())
                .then((res) => {
                    console.log("token: " + token)
                    console.log("getuserData linea 194");
                    console.log(res.status);
                })
                
                 return;
            }
           

            if(this.state.email === ""){
                console.log('hola2')
                toastErrEmail = <Toast visible> Credenciales Incorrectas - Mail </Toast>
                this.setMessage(toastErrEmail);
                this.setState({errorEmail:true});
            }else if(this.state.password === ""){
                console.log('hola');
                toastErrPass = <Toast visible> Credenciales Incorrectas - Pass </Toast>
                this.setMessage(toastErrPass);
                this.setState({errorPass:true});
            }else{
                this.setState({isLoading: true});
                fetch('http://romedicalsistema.com/api/getUserData.php', {
                    method: 'post',
                    body: 'email='+this.state.email+'&password='+this.state.password+'&token='+token,
                    headers: { 'Content-type': 'application/x-www-form-urlencoded' }
                }).then((response) => response.json())
                  .then((res) => {
                      console.log("Acceso correcto");
                      console.log("token: ", token)
                      console.log(res.status);
                     if(res.status == 200){
                         this.props.dispatch({type: 'LOG_IN'})
                         this.props.dispatch({type: 'LOAD_USER_DATA', data: res.data})
                         this.setState({isLoading: false});
                         this.props.dispatch({type: "CHANGE_MAINSCREEN_TAB", payload: "home"});
                         navigation.navigate('MainScreen');
                         toastCorr = <Toast visible> Acceso </Toast>
                         this.setMessage(toastCorr)
                     }else{
                        toastCred = <Toast visible> Credenciales Incorrectas </Toast>
                        this.setState({isLoading: false});
                        this.setState({errorUser:true, errorPass: true});
                        this.setMessage(toastCred);
                        console.log(res);
                    }
                  }).catch((error) => {
                        console.error(error);
                 });
            }    
        });
    }
}

function mapStateToProps(state){
    return{
        email: state.email,
        password: state.password
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 15,
    },
    button: {
        marginBottom: 5,
        backgroundColor: "#5cc0ee",
        borderColor: "#5cc0ee"
    }
});

export default connect(mapStateToProps)(LoginScreen);


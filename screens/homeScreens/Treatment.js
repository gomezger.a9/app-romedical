import React from 'react';
import {View, ScrollView, Image, Dimensions, Text, TouchableHighlight, FlatList, WebView, BackHandler, Linking } from 'react-native';
import Picache from 'picache';
import { connect } from 'react-redux';
import { Button } from '@ant-design/react-native';
import { Ionicons } from '@expo/vector-icons';
import { Permissions, Notifications, Font } from 'expo';
import { relative } from 'path';

                //Descripcion de cada tratamiento con detalle de cada una.

var dimensions = Dimensions.get('window');
var imageWidth = dimensions.width;
var imageHeight = 1;
class Treatment extends React.Component{
    constructor(props){
        super(props);
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    
    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }   
    
    render(){
        return(
                <ScrollView style={{flex:1}}>
                    <Image
                        style={{ height: imageHeight, width: imageWidth}}
                        source={{ uri: this.props.navigation.state.params.eachTreat.headerImage, cache: 'force-cache' }}
                    />

                    {!this.props.navigation.state.params.eachTreat.caracteristicas ?
                    <Text>  </Text> : (
                        <View style={styles.containerView}>
                            <Text style={styles.title}>¿Qué es?</Text>
                            <Text style={styles.text}>{this.props.navigation.state.params.eachTreat.caracteristicas}</Text>
                        </View>
                    )}
                    {!this.props.navigation.state.params.eachTreat.sesiones ? 
                    <Text>  </Text> : (
                        <View style={styles.containerView}>
                            <Text style={styles.title}>Sesiones</Text>
                            <Text style={styles.text}>{this.props.navigation.state.params.eachTreat.sesiones}</Text>
                        </View>
                    )}
                    {!this.props.navigation.state.params.eachTreat.duracion ? 
                    <Text>  </Text> : (
                        <View style={styles.containerView}>
                            <Text style={styles.title}>Duracion</Text>
                            <Text style={styles.text}>{this.props.navigation.state.params.eachTreat.duracion}</Text>
                        </View>
                    )}
                    {!this.props.navigation.state.params.eachTreat.efectosSecundarios ? 
                    <Text></Text> : (
                        <View style={styles.containerView}>
                            <Text style={styles.title}>Post-Tratamiento</Text>
                            <Text style={styles.text}>{this.props.navigation.state.params.eachTreat.efectosSecundarios}</Text>
                        </View>
                    )}
                    {!this.props.navigation.state.params.eachTreat.zonas ? 
                    <Text>  </Text> : (
                        <View style={styles.containerView}>
                            <Text style={styles.title}>Zonas</Text>
                            <Text style={styles.text}>{this.props.navigation.state.params.eachTreat.zonas}</Text>
                        </View>
                    )}
                    {!this.props.navigation.state.params.eachTreat.beneficios ? 
                    <Text>  </Text> : (
                        <View style={styles.containerView}>
                            <Text style={styles.title}>Beneficios</Text>
                            <FlatList
                                data={this.props.navigation.state.params.eachTreat.beneficios}
                                renderItem={({item}) => 
                                    <View style={{ flexDirection: 'row', marginBottom: 10}} >
                                        <Image style={styles.image} source={require('../../assets/images/icon_list.png')} />
                                        <Text style={styles.text}>{item}</Text>
                                    </View>
                                }
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    )}

                    <View style={styles.containerView}>
                        <Text style={styles.title}>Video</Text>
                        <WebView
                            style={{flex:1, height: 250}}
                            javaScriptEnabled={true}
                            source={{uri: 'https://www.youtube.com/watch?v=E-RXXa0e9JA?rel=0&autoplay=0&showinfo=0&controls=0'}}
                        />
                    </View>
                    
                    <View style={{paddingRight:15, paddingLeft: 15 }}>
                        <Button style={{backgroundColor: "#0bd560", marginTop: 5}} onPress={() => {Linking.openURL('whatsapp://send?phone=+541132321372')} }>
                            <Text style={{color: "#FFFFFF"}}>Solicitar turno <Ionicons name="logo-whatsapp" size={20} color="white"/></Text>
                        </Button>
                    </View>         
                     <TouchableHighlight onPress={this.navigateToShifts}  style={styles.TouchableHighlightST}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{fontFamily: 'Gotham-Medium', color: "#FFFFFF", fontWeight: 'bold', alignItems: 'center', fontSize:22, marginRight: 5}}>¡SOLICITÁ TU TURNO!</Text>
                            <Image source= {require('../../assets/images/icon_mouse.png')} style={{width:20, height:20}}></Image>
                        </View>
                    </TouchableHighlight> 
                </ScrollView>
        ) // END RETURN
    } //END RENDER

    /* {!navigation.state.params.eachTreat.video ? (
                            ""
                    ) : ( */
    navigateToShifts = () => {
        console.log("ACA");
        this.props.dispatch({type: 'NAVIGATE_TO_SCREEN', payload: "NewShift"});
        this.props.dispatch({type: 'CHANGE_MAINSCREEN_TAB', payload: "turnos"});
        // this.props.navigation.state.params.mainNavigation.navigate("Main");
        // console.log(this.props.navigation.state.params.mainNavigation.reset(null));

    } //END NAVIGATIONSHIFFTS
}// End React.Components
imageHeight = (imageWidth*100/750)/100 * 250;
const styles = {
    containerView: {
        paddingRight: 15,
        paddingLeft: 15,
        marginTop: 25,
        marginBottom:25 
    },
    title: {
        fontFamily: 'Gotham-Medium',
        marginBottom: 10,
        fontWeight: 'bold',
        color:'#65BBE9',
        fontSize: 23,
    },
    text: {
        fontFamily: 'Gotham-Light',
        textAlign: 'auto',
        fontSize: 20,
        padding:11,
        
    },
    image: {
        width:7.5, 
        height:6, 
        marginRight:5, 
        marginTop: 3, 
        padding:4,
        position: 'relative',
        top:10
    },
    TouchableHighlightST: {
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        marginBottom:25,
        paddingTop:15,
        paddingBottom:15,
        backgroundColor:'#65BBE9',
        alignItems: 'center',
        borderRadius:2
    }
}

const mapStateToprops = state => {
    return{
        affections: state.affections,
    }
}

export default connect(mapStateToprops)(Treatment);
import React from 'react';
import {View, ScrollView, Image, Dimensions, Text, TouchableOpacity, BackHandler, StyleSheet } from 'react-native';
import { List } from '@ant-design/react-native';
import { colors } from 'react-native-elements';

const Item = List.Item;
const Brief = Item.Brief;

var dimensions = Dimensions.get('window');
var imageWidth = dimensions.width;
var imageHeight = 1;

               // Productos de cada afeccion.

export default class Treatments extends React.PureComponent{
    constructor(props){
        super(props);
        this.state = {
            refreshing: false
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    }
    
    onRefresh = () => {
        this.setState({ refreshing: true });
        fetchData().then(() => {
            this.setState({ refreshing: false })
        }) 
    }
    render(){
        return(
            <View style={{flex: 1}}>
                {/* <Button style={{flex: 1, flexDirection: 'column', alignItems: 'stretch', backgroundColor:'#1E6738',}} title="< Atrás" onPress={()=> { this.props.navigation.goBack(null);}}/> */}
                <ScrollView>
                    <List>
                    {
                        this.props.navigation.state.params.treatments.map((eachTreat, key) => {
                            return <TouchableOpacity key={key} onPress={() => { this.props.navigation.navigate("Treatment",{ eachTreat, mainNavigation: this.props.navigation }) }}>
                                        <Image
                                            style={styles.imageStyle}
                                            source={{ uri: eachTreat.headerImage, cache: 'force-cache' }}
                                        />
                                    </TouchableOpacity>
                        })
                    }
                    </List>
                </ScrollView>
            </View>
        );
    }
}
imageHeight = (imageWidth*100/750)/100 * 250;
const styles = StyleSheet.create({
    imageStyle: {
        height: imageHeight,
        width:  imageWidth,
        backgroundColor: 'grey', 
        margin: StyleSheet.hairlineWidth
    }
});



import React from 'react';
import {View, ScrollView, Image, Dimensions, TouchableHighlight, BackHandler, StyleSheet } from 'react-native';
import { List } from '@ant-design/react-native';
import axios from "axios";

import { connect } from 'react-redux';

const Item = List.Item;
const Brief = Item.Brief;


               //Productos, Home Screen de Afecciones.


class Affections extends React.Component {
    static navigationOptions = {
        headerLeft: null,
    };

    constructor(props){
        super(props);
        this.props.navigation.navigationOptions = {
            gesturesEnabled: false,
            cardStack: {
                gesturesEnabled: false,
            },
        }
    }

    componentDidMount() {
        axios.get(`http://romedicalsistema.com/api/getTreatmentsStructure.php`)
        .then(res => {
            this.setState({affections: res.data.data});
        })
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }  

    // onButtonPress = () => {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    //     // then navigate
    //     navigate('NewScreen');
    // }
      
    handleBackButton = () => {
        BackHandler.exitApp()  
        return true;
    } 

    render(){
        return (
            <View style={{flex: 1}}>
                <ScrollView
                    style={{ flex: 1, backgroundColor: '#f5f5f9' }}
                    automaticallyAdjustContentInsets={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                        <List>
                            {
                                this.props.affections.map((eachAfecc, key) => {
                                    return <TouchableHighlight key={key} onPress={() => {this.props.navigation.navigate("Treatments",{ treatments: eachAfecc.treatments, mainNavigation: this.props.navigation })}}>
                                                <Image
                                                    style={styles.imageStyle}
                                                    source={{ uri: eachAfecc.image, cache:'force-cache' }}
                                                />
                                            </TouchableHighlight>
                                })
                            }
                        </List>
                </ScrollView>
            </View>
        );
    } // End Render
} // End React.Component

var dimensions = Dimensions.get('window');
var imageWidth = dimensions.width;

const imageHeight = (imageWidth*100/750)/100 * 250;

const styles = StyleSheet.create({
    
    imageStyle: {
        height: imageHeight, 
        width:  imageWidth,
        backgroundColor: 'grey', 
        margin: StyleSheet.hairlineWidth
    }
})

const mapStateToprops = state => {
    return{
        affections: state.affections,
    }
}

export default connect(mapStateToprops)(Affections);
import React, {Component} from 'react';
import {View, Text, Image, ScrollView, StyleSheet, BackHandler, Linking, RefreshControl} from 'react-native';
import { connect } from 'react-redux';
import { Card, Button } from '@ant-design/react-native';
import NavigationService from '../../NavigationService';
import { Ionicons } from '@expo/vector-icons';
import TimerMixin from 'react-timer-mixin';


class ListShifts extends Component{
    constructor(props){
        super(props);
        this.state = {
            refreshing   : false,
            isLoading    : false,
            fetchShifts  : false
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        BackHandler.exitApp()  
        return true;
    } 
    render(){
        console.log(this.props.screenToNavigate);
        if(this.props.screenToNavigate == "NewShift"){
            this.props.dispatch({type: 'NAVIGATE_TO_SCREEN', payload: ""})
            this.props.navigation.navigate("NewShift");
        }
        if(this.props.shifts == null){
            if(!this.props.userIsLogged){
                return(
                    <View style={{backgroundColor: '#FFFFFF', paddingRight: 5, paddingLeft: 5, marginTop:15}}>
                        <Text style={{ textAlign: 'center',marginBottom: 15, color: "grey"}}>Para solicitar un turno deberá registrarse e identificarse.</Text>
                        <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goLogIn}>Identificarse</Button>
                    </View>
                );
            }else{
                return (
                    <ScrollView 
                    style={{ backgroundColor: '#FFFFFF', paddingTop: 10, paddingRight:15, paddingLeft: 15 }}
                    refreshControl = {
                        <RefreshControl 
                            refreshing = {this.state.refreshing}
                            onRefresh = {this._onRefresh}
                        />
                    }>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={styles.title}>
                                No se registran turnos pendientes
                            </Text>
                        </View>
                        <View style={styles.ViewST}>
                            <Button 
                                style={styles.buttonST} 
                                containerViewStyle={{width: '100%', marginLeft: 0}}
                                type="primary" 
                                onPress={() => {this.props.navigation.navigate("NewShift")}}>Solicitar turno</Button>         
                            <Button style={{backgroundColor: "#0bd560", marginTop: 5}} onPress={() => {Linking.openURL('whatsapp://send?phone=+541132321372')} }>
                                <Text style={{color: "#FFFFFF"}}>Whatsapp  </Text><Ionicons name="logo-whatsapp" size={20} color="white"/>
                            </Button>
                        </View> 
                    </ScrollView>
                )
                }
        }else{
            return(
                <ScrollView style={{ backgroundColor: '#FFFFFF', paddingTop: 10}}
                            refreshControl = {
                        <RefreshControl 
                            refreshing = {this.state.refreshing}
                            onRefresh = {this._onRefresh}
                        />
                    }>
                    {                        
                         this.props.shifts.map((eachShift, key) => {
                            var day = "";

                            switch(eachShift.dayofweekstring){
                                case "Lun":
                                    day = "Lunes";
                                break;
                                case "Mar":
                                    day = "Martes";
                                break;
                                case "Mie":
                                    day = "Miércoles"
                                break;
                                case "Jue":
                                    day = "Jueves"
                                break;
                                case "Vie":
                                    day = "Viernes";
                                break;
                                case "Sab":
                                    day = "Sábado";
                                break;
                                case "Dom":
                                    day = "Domingo";
                                break;
                            }

                            return eachShift.status =='ABSENT' || eachShift.status == 'BLOCK' || eachShift.status == 'CANCELED' ? 
                            <View key={key}>   </View>
                            : <ScrollView style={{ backgroundColor: '#FFFFFF', flex: 1, padding:10, marginTop: 5, marginBottom: -5 }} key={key}>
                                        <Card>
                                            <Card.Header
                                                title={eachShift.productName}
                                                // extra="this is extra"
                                            />
                                            <Card.Body>
                                            <View style={{ }}>
                                                <Text style={{ marginLeft: 16 }}>Sucursal: {eachShift.sucursal}</Text>
                                                <Text style={{ marginLeft: 16 }}>{eachShift.operadorNombre}</Text>
                                                <Text style={{ marginLeft: 16 }}>{day} {eachShift.date}</Text>
                                                <Text style={{ marginLeft: 16 }}>{eachShift.time} HS</Text>
                                            </View>
                                            </Card.Body>
                                        </Card>
                                </ScrollView>
                        })
                    }
                    
                    <View style={styles.ViewST}>
                        <Button 
                        style={styles.buttonST} 
                        containerViewStyle={{width: '100%', marginLeft: 0}}
                        type="primary" 
                        onPress={() => {this.props.navigation.navigate("NewShift")}}>Solicitar turno</Button>         
                        <Button style={{backgroundColor: "#0bd560", marginTop: 5}} onPress={() => {Linking.openURL('whatsapp://send?phone=+541132321372')} }>
                            <Text style={{color: "#FFFFFF"}}>Whatsapp  </Text><Ionicons name="logo-whatsapp" size={20} color="white"/>
                        </Button>
                    </View>         
            </ScrollView>
            );
        }
    }
    goLogIn = () => {
        NavigationService.navigate("LoginScreen");
        // this.props.screenProps.mainNavigation.navigate("LoginScreen");
    }

   
    _onRefresh = () => {
        this.setState({ 
            refreshing   : true,
            isLoading    : true,
            fetchShifts  : true
        });
        TimerMixin.setTimeout(() => {
        this.fetchData()
            .then(() => {
            this.setState({ 
                refreshing   : false,
                fetchShifts  : false,
                isLoading    : false
            })
        })
        .catch((err) => {
            console.log("Fallo", err)
        })
        }, 2500)
    }

    async fetchData() {
        this.setState({isLoading: true});
        console.log('token: ' + this.props.token)
        return fetch('http://romedicalsistema.com/api/getUserShifts.php', {
            method: 'post',
            body: 'clientId='+this.props.clientId+'&token='+this.props.token,
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {
            if(res.status == 200){
                this.props.dispatch({type: 'LOAD_SHIFTS', payload: res.data})
            }
            this.setState({fetchShifts: false});
          }).catch((error) => {
                console.error(error);
                this.setState({fetchShifts: true});    
         });
    }

}
const styles = StyleSheet.create({
    contDescrip: {
      height: 60,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    ViewST: {
        // flex:1, 
        // flexDirection: 'column', 
        // justifyContent: 'center', 
        // alignItems: 'stretch', 
        paddingRight:15, 
        paddingLeft:15,
        marginTop: 10,
    },
    buttonST: {
        backgroundColor: "#5cc0ee", 
        borderColor: "#5cc0ee",
    },
    leftContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    rightContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    rightIcon: {
      height: 10,
      width: 10,
      resizeMode: 'contain',
      backgroundColor: 'white',
    },
    title: {
      fontFamily: 'Gotham-Medium',
      fontWeight: 'bold',
      width:150,
      height:150,
      fontSize: 21,
      textAlign: 'center',
    }
  });

const mapStateToprops = state => {
    return{
        shifts: state.shifts,
        userIsLogged: state.userIsLogged,
        screenToNavigate: state.screenToNavigate,
        contactNumber: state.contactNumber,
        token: state.token,
        clientId: state.clientId 
    }
}

export default connect(mapStateToprops)(ListShifts);
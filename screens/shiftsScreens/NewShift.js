import React from 'react';
import {View, TouchableOpacity, Keyboard, BackHandler, Text, Dimensions, ActivityIndicator } from 'react-native';

import { List, Picker, TextareaItem, DatePicker, Button } from '@ant-design/react-native';
import TimerMixin from 'react-timer-mixin';

import { connect } from 'react-redux';
import NavigationService from '../../NavigationService';

// const Item = List.Item;
// const Brief = Item.Brief;
class NewShift extends React.Component{
    constructor(props) {
        super(props);
        navigation = this.props.navigation;
        this.handleBackButton = this.handleBackButton.bind(this);
    }
    state = {
        dataHours: [
            { label: 'Por la mañana',value: 'MAÑANA'},
            { label: 'Por la tarde',value: 'TARDE'},
          ],
          sucursales: [
            {label: 'Martinez', value: 'Martinez'},
            {label: 'Nordelta', value: 'Nordelta'},
            {label: 'Palermo', value: 'Palermo'}
          ],
          treatment: "Seleccionar",
          date: "Seleccionar",
          hour: "Seleccionar",
          sucursal: "Seleccionar",
          treatmentName: "Seleccionar",
          comment: "",
          notificationText: "",
          isLoading: false
    }
    componentDidMount() {
        treatments = [];
        this.props.treatments.forEach( function(treatment){
            treatment = { label: treatment.nombre, value: treatment.tratamientoId }
            treatments.push(treatment);
        });
        this.setState({treatments});
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    setMessage(message){
        this.setState({notificationText: message})
        TimerMixin.setTimeout(
            () => { this.setState({notificationText: ""}) },
            2000
        );
    }

    onChangeTreat = (value) => {
        this.setState({treatment: value});
        var treatmentLabel = "";
        this.state.treatments.forEach( function(treatment){
            if(treatment.value == value){
                treatmentLabel = treatment.label;
            }
        });
        this.setState({treatmentName: treatmentLabel})
    }
    onChangeDate = (value) => {
        this.setState({date: value.toLocaleDateString() }); 
    }
    onChangeHour = (value) => {
        this.setState({hour: value }); 
    }
    onChangeSucursal = (value) => {
        this.setState({sucursal: value }); 
    }

    goLogIn = () => {
        NavigationService.navigate("LoginScreen");
        // this.props.navigation.navigate("LoginScreen");
    }

    render() {
        const dateMin = new Date();
        const dateMax = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
        let screenHeight = Dimensions.get("window").height;

        if(!this.props.userIsLogged){
            return(
                <View style={{paddingRight: 5, paddingLeft: 5, marginTop:15}}>
                    <Text style={{ textAlign: 'center', marginBottom: 15, color: "grey"}}>Para solicitar un turno deberá registrarse e identificarse.</Text>
                    <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goLogIn}>Identificarse</Button>
                </View>
            );
        }else{
            return (
                <View style={{ paddingRight: 5, paddingLeft: 5, height: screenHeight }}>
                    <List style={{height: screenHeight, paddingBottom: 200}}>
                        <Picker
                            data={this.state.treatments}
                            cols={1}
                            okText="Seleccionar"
                            dismissText="Cancelar"
                            extra={this.state.treatmentName}
                            onOk={this.onChangeTreat}
                        >
                            <List.Item arrow="horizontal">Tratamiento</List.Item>
                        </Picker>
                        <Picker
                            data={this.state.sucursales}
                            cols={1}
                            okText="Seleccionar"
                            dismissText="Cancelar"
                            extra={this.state.sucursal}
                            onOk={this.onChangeSucursal}
                        >
                            <List.Item arrow="horizontal">Sucursal</List.Item>
                        </Picker>
                        <DatePicker
                            value={this.state.value}
                            mode="date"
                            minDate={dateMin}
                            maxDate={dateMax}
                            onOk={this.onChangeDate}
                            format="HH:mm"
                            dismissText="Cancelar"
                            okText="Seleccionar"
                            extra={this.state.date}
                            indicatorStyle={null}
                            itemStyle={null}
                        >
                            <List.Item arrow="horizontal">Fecha</List.Item>
                        </DatePicker>
                        <Picker
                            data={this.state.dataHours}
                            cols={1}
                            okText="Seleccionar"
                            dismissText="Cancelar"
                            extra={this.state.hour}
                            onOk={this.onChangeHour}
                        >
                            <List.Item arrow="horizontal">Hora</List.Item>
                        </Picker>
                        <TextareaItem
                            rows={7}
                            placeholder="Descripción de la consulta"
                            onChangeText={(el) => {this.state.comment = el; }}
                        ></TextareaItem>
                        <Button disabled={this.state.isLoading} type="primary" style={{backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.onSubmit}>Enviar</Button>
                        <ActivityIndicator style={{marginTop: 5}} size="large" color="#5cc0ee" animating={ this.state.isLoading }/>
                        <Text style={{color: 'grey', paddingBottom:screenHeight, textAlign: 'center'}}>
                            {this.state.notificationText}
                        </Text>
                    </List>
                </View>
            );
        }
    }
    onSubmit = () =>{
        if(this.state.treatment == "Seleccionar"){
            this.setMessage("Debe seleccionar un tratamiento");
            return;
        }else if(this.state.sucursal == "Seleccionar"){
            this.setMessage("Debe seleccionar una sucursal");
            return;
        }else if(this.state.date == "Seleccionar"){
            this.setMessage("Debe seleccionar un una fecha");
            return;
        }else if(this.state.hour == "Seleccionar"){
            this.setMessage("Debe seleccionar un horario");
            return;
        }

        this.setState({isLoading: true});
        fetch('http://romedicalsistema.com/api/setShiftRequest.php', {
            method: 'post',
            body: 'clientId='+this.props.clientId+
                  '&token='+this.props.token+
                  '&treatment='+this.state.treatment+
                  '&sucursal='+this.state.sucursal+
                  '&time='+this.state.hour+
                  '&dateRequested='+this.state.date+
                  '&comment='+this.state.comment,
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {
              console.log(res);
            if(res.status == 200){
                // this.setMessage("El turno ha sido solicitado exitosamente!");
                this.setState({isLoading: false});
                this.props.navigation.navigate("ListShifts");
            }else{
                this.setMessage("Error, el turno no ha podido ser solicitado en este momento.");
                this.setState({isLoading: false});
            }
          }).catch((error) => {
                console.error(error);
                this.setMessage("Error, el turno no ha posido ser solicitado.");
                this.setState({isLoading: false});
         }); 
    }
    goLogIn = () => {
        NavigationService.navigate("LoginScreen");
        // this.props.screenProps.mainNavigation.navigate("LoginScreen");
    }
}

const mapStateToprops = state => {
    return{
        user: state.user,
        name: state.name,
        userIsLogged: state.userIsLogged,
        token: state.token,
        clientId: state.clientId,
        treatments: state.treatments
    }
}

export default connect(mapStateToprops)(NewShift);
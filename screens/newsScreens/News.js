import React from 'react';
import {Text, View, ScrollView, StyleSheet, BackHandler, Image} from 'react-native';
import { connect } from 'react-redux';

class News extends React.Component{
    
    constructor(props) {
        super(props);
        this.handleBackButton = this.handleBackButton.bind(this);
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }
     
    render(){
        height = this.getParameterByName(this.props.navigation.state.params.news.imagen, "h");
        width = this.getParameterByName(this.props.navigation.state.params.news.imagen, "w");

        imageWidth = 300;
        imageHeight = height * (imageWidth * 100 / width) /100;
        return(
            
            <ScrollView style={{ backgroundColor: '#FFFFFF', flex: 1, padding:15 }}>
                <View style={{ justifyContent: 'center',
                        alignItems: 'center',}}>
                <Image
                    source={{uri: this.props.navigation.state.params.news.imagen, cache:'force-cache'}}
                    style={{ width: imageWidth, height: imageHeight }}
                />
                </View>
                <View style={styles.contDescrip}>
                    <View style={styles.leftContainer}>
                        <Text>
                            <Text style={styles.title}> {this.props.navigation.state.params.news.titulo} </Text>
                            <Text style={styles.text}> {"\n"} {this.props.navigation.state.params.news.mes} </Text>
                        </Text>
                    </View>
                </View>
                <View style={{ paddingRight: 5,paddingLeft: 5, marginBottom: 75}}>
                    <Text style={styles.textDesc}>
                        {this.props.navigation.state.params.news.descripcion}
                    </Text>
                </View>
        </ScrollView>
        );
    }
    getParameterByName(url,name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

const styles = StyleSheet.create({
    contDescrip: {
      height: 60,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: 5,
      paddingLeft: 5
    },
    leftContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    rightContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    rightIcon: {
      height: 10,
      width: 10,
      resizeMode: 'contain',
      backgroundColor: 'white',
    },
    title: {
        marginBottom: 10,
        fontWeight: 'bold',
        color:'#65BBE9',
        fontSize: 23,
        textAlign: 'left'
    },
    text: {
        fontFamily: 'Gotham-Light',
        textAlign: 'left',
        fontSize: 20,
        padding:11
    },
    textDesc: {
        flex: 1,
        flexDirection: 'row',
        fontFamily: 'Gotham-Light',
        textAlign: 'justify',
        fontSize: 20,
        padding:11,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
  });

  const mapStatetoprops = state => {
    return {
        userIsLogged: state.userIsLogged
    }
  }

  export default connect(mapStatetoprops)(News);
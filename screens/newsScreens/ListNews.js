import React from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, BackHandler, Image, RefreshControl, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';


const dim = Dimensions.get('window');

class ListNews extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            refreshing : false,
            isLoading  : false,
            fetchNews  : false
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        BackHandler.exitApp()  
        return true;
    } 
    
    render(){
        
        if(this.props.news == null) {
            return (
                <Text>No news</Text>
            )
        }else {
            return (
                <ScrollView style={styles.scrollViewStyles}
                    refreshControl = {
                        <RefreshControl 
                            refreshing = {this.state.refreshing}
                            onRefresh = {this._onRefresh}
                        />
                    }>
                    {
                        this.props.news.map((eachNews, key) => {
                            return (
                            <TouchableOpacity style={styles.cardStyle} 
                            key={key} 
                            onPress={() => {
                                this.props.navigation.navigate("News", {news: eachNews})
                            }}>
                                <View style={styles.containerViewStyle}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image
                                            source={{uri: eachNews.imagen, cache: 'force-cache'}}
                                            style={{ flex: 1, width: 350, height: 350, resizeMode: 'contain' }}
                                        />
                                    </View>
                                        <View style={styles.leftContainer}>
                                            <Text>
                                                <Text style={[styles.title, { width: dim.width / 2}]}> {eachNews.titulo}  </Text>
                                                <Text style= {styles.text}> {"\n"} {eachNews.mes} </Text>
                                            </Text>                                                                                                                    
                                    </View>
                                </View>
                            </TouchableOpacity>)
                        })
                    }
                </ScrollView>
            );
        }
    }

    _onRefresh = () => {
        this.setState({ 
            refreshing : true,
            isLoading  : true,
            fetchNews  : true
        });
        TimerMixin.setTimeout(() => {
        this.fetchData()
            .then(() => {
            this.setState({ 
                refreshing : false,
                fetchNews  : false,
                isLoading  : false
            })
        })
        .catch((err) => {
            console.log("Fallo", err)
        })
        }, 2500)
    }

    async fetchData() {
        this.setState({isLoading: true});
        return fetch('http://romedicalsistema.com/api/getNovedades.php', {
            method: 'get',
        }).then((response) => response.json())
          .then((res) => {
            this.props.dispatch({ type: 'LOAD_NEWS', payload: res.data });
            this.setState({fetchNews: false});
          }).catch((error) => {
                console.error(error);
                this.setState({fetchNews: true});
        });
    }
}
const styles = StyleSheet.create({
    cardStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginTop: 5,
        marginBottom: 5
    },
    contDescrip: {
      height: 60,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: 25,
      paddingLeft: 25
    },
    leftContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    rightIcon: {
      height: 10,
      width: 10,
      resizeMode: 'contain',
      backgroundColor: 'white',
    },
    title: {
        fontFamily: 'Gotham-Medium',
        //marginBottom: 10,
        fontWeight: 'bold',
        color:'#65BBE9',
        fontSize: 21,
        textAlign: 'left',
        
    },
    text: {
        fontFamily: 'Gotham-Light',
        
        textAlign: 'left',
        fontSize: 21,
        padding:11
    
    },
    scrollViewStyles: {
        backgroundColor: '#FFFFFF', 
        paddingTop: 30
    },
    containerViewStyle: {
        backgroundColor: '#FFFFFF', 
        flex: 1, 
        padding: 15, 
        marginBottom: 25
    }
  });

const mapStateToprops = state => {
    return{user: state.user, password: state.password, news: state.news}
}

export default connect(mapStateToprops)(ListNews);
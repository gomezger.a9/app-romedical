import React from 'react';
import {View, Text, Image, Dimensions, ActivityIndicator} from 'react-native';
import { Asset, Font } from 'expo';
Asset;
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';
import axios from "axios";

// import clear from 'react-native-clear-app-cache'

const screenWidth = Dimensions.get('window').width;

function cacheImages(images) {
    return images.map(image => {
      if (typeof image === 'string') {
        return Image.prefetch(image);
      } else {
        return Asset.fromModule(image).downloadAsync();
      }
    });
}


class SplashScreen extends React.PureComponent {
    constructor(props){
        super(props);
        navigation = this.props.navigation;
        this.state = {
            isLoading: false,
            fetchNews: false,
            fetchBenefits: false,
            fetchTreatments: false,
            fetchUserData: false,
            fetchShifts: false,
            fontLoaded: false
        }
    }

    render() {
        return (
            <View style={{flex: 1,justifyContent: 'center',alignItems: 'center'}}>
            <Image
                style={{
                    width: 288,
                    height: 36,
                }}
                source={require('../assets/images/logoheader.png')}
            />
            <ActivityIndicator style={{paddingTop:15}} animating={ this.state.isLoading } size="large" color="#5cc0ee" />
            </View>
        );
    }

    componentWillMount(){
        this.setState({isLoading: true});
        fetch('http://romedicalsistema.com/api/getNovedades.php', {
            method: 'get',
        }).then((response) => response.json())
          .then((res) => {
            this.props.dispatch({ type: 'LOAD_NEWS', payload: res.data });
            this.setState({fetchNews: true});
            this.checkFetchs();
          }).catch((error) => {
                console.error(error);
                this.setState({fetchNews: true});
                this.checkFetchs();
        });
        
        fetch('http://romedicalsistema.com/api/getBeneficios.php', {
            method: 'get',
        }).then((response) => response.json())
          .then((res) => {
            this.props.dispatch({ type: 'LOAD_BENEFITS', payload: res.data })
            this.setState({fetchBenefits: true});
            this.checkFetchs();
        }).catch((error) => {
                console.error(error);
                this.setState({fetchBenefits: true});
                this.checkFetchs();
        });

        fetch('http://romedicalsistema.com/api/getTreatments.php', {
            method: 'get',
        }).then((response) => response.json())
          .then((res) => {
            this.props.dispatch({ type: 'LOAD_TREATMENTS', payload: res.data })
            this.setState({fetchTreatments: true});
            this.checkFetchs();
        }).catch((error) => {
                console.error(error);
                this.setState({fetchTreatments: true});
                this.checkFetchs();
        });

        fetch('http://romedicalsistema.com/api/getTreatmentsStructure.php', {
            method: 'get',
        }).then((response) => response.json())
          .then((res) => {
                // console.log(res.data);
                this.props.dispatch({ type: 'LOAD_AFFECTIONS', payload: res.data })
                this.setState({fetchTreatments: true});
                this.checkFetchs();
          }).catch((error) => {
                console.error(error);
                this.setState({fetchTreatments: true});
                this.checkFetchs();
        });

        fetch('http://romedicalsistema.com/api/getUserData.php', {
            method: 'post',
            body: 'email='+this.props.email+'&token='+this.props.token,
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {
            //   console.log('email='+this.props.email+'&token='+this.props.token);
            //   console.log(res.data);
            if(res.status == 200){
                this.props.dispatch({type: 'LOAD_USER_DATA', data: res.data})
            }
            this.setState({fetchUserData: true});
            this.checkFetchs();
          }).catch((error) => {
                console.error(error);
                this.setState({fetchUserData: true});
                this.checkFetchs();
         });
         
         fetch('http://romedicalsistema.com/api/getUserShifts.php', {
            method: 'post',
            body: 'clientId='+this.props.clientId+'&token='+this.props.token,
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {
            if(res.status == 200){
                this.props.dispatch({type: 'LOAD_SHIFTS', payload: res.data})
            }
            this.setState({fetchShifts: true});
            this.checkFetchs();
          }).catch((error) => {
                console.error(error);
                this.setState({fetchShifts: true});
                this.checkFetchs();
         });

         Font.loadAsync({
            'Gotham-Medium': require('../assets/fonts/Gotham-Medium-Regular.ttf'),
            'Gotham-Light': require('../assets/fonts/Gotham-Light.ttf'),
          }).then(()=>{
                this.setState({ fontLoaded: true });
                this.checkFetchs();
          });
    }

    checkFetchs = () =>{
        // console.log("CHECKFETCHS");
        // console.log(this.state);
        if(this.state.fetchNews == true &&
            this.state.fetchBenefits == true &&
            this.state.fetchTreatments == true &&
            this.state.fetchUserData == true &&
            this.state.fetchShifts == true &&
            this.state.fetchTreatments == true &&
            this.state.fontLoaded == true){
                this.setState({isLoading: false});
                if(this.props.userIsLogged){
                    navigation.navigate("MainScreen");
                }else{
                    navigation.navigate("LoginScreen");
                }
            }
    }

    onBackButtonPressAndroid = () => {
        return false;
    };
}

const mapStateToprops = state => {
    return{
        userIsLogged: state.userIsLogged,
        pictures: state.pictures,
        token: state.token,
        email: state.email,
        clientId: state.clientId
    }
}

export default connect(mapStateToprops)(SplashScreen);
import React from 'react';
import {Text, View, ScrollView, StyleSheet, BackHandler, Image} from 'react-native';
import { Button } from '@ant-design/react-native';
import { connect } from 'react-redux';
import NavigationService from '../../NavigationService';

class Benefit extends React.Component{
    
    constructor(props) {
        super(props);
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }    

    getParameterByName(url,name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    goLogIn = () => {
        NavigationService.navigate("LoginScreen");
        // this.props.screenProps.mainNavigation.navigate("LoginScreen");
    }
    obtenerCupon = (beneficio) => {
        console.log("AQUI: ");
        console.log(this.props.token);
        console.log(this.props.clientId);
        console.log(beneficio.beneficioId);
        fetch('http://romedicalsistema.com/api/setBeneficioCounterView.php', {
            method: 'post',
            body: 'clientId='+this.props.clientId+'&token='+this.props.token+'&beneficioId='+beneficio.beneficioId,
            headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }).then((response) => response.json())
          .then((res) => {        
            if(res.status == 200){
                console.log('deberia estar: '+res.status);
            }
          }).catch((error) => {
                console.error(error);
         }); 

        this.props.navigation.navigate("Cupon", { eachBenefit: beneficio })
    }
    render(){
        if(!this.props.userIsLogged){
            return(
                <View style={{paddingRight: 5, paddingLeft: 5, marginTop:15}}>
                    <Text style={{ textAlign: 'center', marginBottom: 15, color: "grey"}}>Para acceder a los beneficios deberá registrarse e identificarse.</Text>
                    <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onClick={this.goLogIn}>Identificarse</Button>
                </View>
            );
        }else{
            height = this.getParameterByName(this.props.navigation.state.params.eachBenefit.imagen, "h");
            width = this.getParameterByName(this.props.navigation.state.params.eachBenefit.imagen, "w");
    
            imageWidth = 300;
            imageHeight = height * (imageWidth * 100 / width) /100;

            return(
                <ScrollView style={{ backgroundColor: '#FFFFFF', flex: 1, padding:15 }}>
                    <View style={{ justifyContent: 'center',
                            alignItems: 'center',}}>
                    <Image
                        source={{uri: this.props.navigation.state.params.eachBenefit.imagen, cache: 'force-cache'}}
                        style={{ width: imageWidth, height: imageHeight }}
                    />
                    </View>
                    <View style={styles.contDescrip}>
                        <View style={styles.leftContainer}>
                            <Text>
                                <Text style={styles.title}> {this.props.navigation.state.params.eachBenefit.titulo}  </Text>
                                <Text style={styles.text}> {"\n"} {this.props.navigation.state.params.eachBenefit.mes} </Text>
                            </Text>
                        </View>
                    </View>
                    <View>
                        <Text style={[styles.textDesc, {textAlign:'justify'}]}>
                            {this.props.navigation.state.params.eachBenefit.descripcion}
                        </Text>
                    </View>
                    <Button type="primary" style={styles.button} onPress={() => this.obtenerCupon(this.props.navigation.state.params.eachBenefit)}>Obtener cupón</Button>
            </ScrollView>
            );
        }
    }
}

const styles = StyleSheet.create({
    contDescrip: {
      height: 60,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: 5,
      paddingLeft: 5
    },
    leftContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    rightContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    rightIcon: {
      height: 10,
      width: 10,
      resizeMode: 'contain',
      backgroundColor: 'white'
    },
    button: {
        marginBottom: 75,
        backgroundColor: "#5cc0ee",
        borderColor: "#5cc0ee"
    }, 
    title: {
        fontFamily: 'Gotham-Medium',
        marginBottom: 10,
        fontWeight: 'bold',
        color:'#65BBE9',
        fontSize: 23
    },
    text: {
        fontFamily: 'Gotham-Light',
        fontSize: 20,
        padding:11,
        justifyContent: 'center'
    },
    textDesc: {
        flex: 1,
        flexDirection: 'row',
        fontFamily: 'Gotham-Light',
        textAlign: 'justify',
        fontSize: 20,
        padding:11,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
  });

const mapStateToprops = state => {
    return{
        userIsLogged: state.userIsLogged,
        token: state.token,
        clientId: state.clientId
    }
}

export default connect(mapStateToprops)(Benefit);
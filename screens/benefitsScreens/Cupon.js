import React from 'react';
import {Text, View, StyleSheet, BackHandler} from 'react-native';
import Picache from 'picache';
import { connect } from 'react-redux';
//import console = require('console');

class Cupon extends React.Component{
    
    constructor(props) {
        super(props)
        this.handleBackButton = this.handleBackButton.bind(this);
    }
    
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }
    // handleBackButton = () => {
    //     //this.props.navigation.navigate("Benefit", {eachBenefit: this.props.navigation.state.params.eachBenefit})
    //     this.props.navigation.goBack(null);
    //     console.log("aber aki")
    //     //return true;
    // }    
    render(){
        height = this.getParameterByName(this.props.navigation.state.params.eachBenefit.cupon, "h");
        width = this.getParameterByName(this.props.navigation.state.params.eachBenefit.cupon, "w");
        imageWidth = 300;
        imageHeight = height * (imageWidth * 100 / width) /100;
        if(!this.props.userIsLogged){
            return(
                <View style={{paddingRight: 5, paddingLeft: 5, marginTop:15}}>
                    <Text style={{ textAlign: 'center', marginBottom: 15, color: "grey"}}>Para acceder a los beneficios deberá registrarse e identificarse.</Text>
                    <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goLogIn}>Identificarse</Button>
                </View>
            );
        }else{
            return(
                <View style={{ backgroundColor: '#FFFFFF', flex: 1, padding:15, marginBottom:25 }}>
                    <Picache
                        source={{uri: this.props.navigation.state.params.eachBenefit.cupon}}
                        style={{
                            width: imageWidth,
                            height: imageHeight 
                        }}
                    />
                </View>
            )
        }
    }

    getParameterByName(url,name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

const styles = StyleSheet.create({

});

const mapStateToprops = state => {
    return{
        userIsLogged: state.userIsLogged,
    }
}

export default connect(mapStateToprops)(Cupon);
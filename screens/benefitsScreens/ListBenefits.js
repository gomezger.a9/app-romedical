import React from 'react';
import {View, Text, Dimensions, ScrollView, StyleSheet, TouchableOpacity, BackHandler, Image, RefreshControl} from 'react-native';
import { connect } from 'react-redux';
import { Button } from '@ant-design/react-native';
import NavigationService from '../../NavigationService';
import TimerMixin from 'react-timer-mixin';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
class ListBenefits extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            refreshing : false,
            isLoading  : false,
            fetchBenefits  : false
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        BackHandler.exitApp()  
        return true;
    }   

    render(){
        if(!this.props.userIsLogged){
            return(
                <View style={{ paddingRight: 5, paddingLeft: 5, marginTop:15}}>
                    <Text style={{ textAlign: 'center',marginBottom: 15, color: "grey"}}>Para acceder a los beneficios deberá registrarse e identificarse.</Text>
                    <Button type="primary" style={{ backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goLogIn}>Identificarse</Button>
                </View>
            );
        }else{
            return(
                <ScrollView style={{ backgroundColor: '#FFFFFF', paddingTop: 30}}
                            refreshControl = {
                        <RefreshControl 
                            refreshing = {this.state.refreshing}
                            onRefresh = {this._onRefresh}
                        />
                    }>
                    {
                        this.props.benefits.map((eachBenefit, key) => {
                            
                            height = this.getParameterByName(eachBenefit.imagen, "h");
                            width = this.getParameterByName(eachBenefit.imagen, "w");
                            imageHeight = height * (300 * 100 / width) /100;
                            
                            return <TouchableOpacity style={styles.cardStyle} key={key} onPress={() => { this.props.navigation.navigate("Benefit",{ eachBenefit })}}>
                                    <View style={{ backgroundColor: '#FFFFFF', flex: 1, padding:15, marginBottom:25 }}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center',}}>
                                        <Image
                                            source={{uri: eachBenefit.imagen, cache: 'force-cache'}}
                                            style={{ width: 350, height: imageHeight }}
                                        />
                                        </View>
                                            <View style={styles.leftContainer}>
                                                <Text>
                                                    <Text style={styles.title}> {eachBenefit.titulo} </Text>
                                                    <Text style={styles.text}> {"\n"} {eachBenefit.mes} </Text>
                                                </Text>    
                                            </View>                                        
                                        </View>
                                </TouchableOpacity>
                        })
                    }    
            </ScrollView>
            );
        }
    }
    _onRefresh = () => {
        this.setState({ 
            refreshing     : true,
            isLoading      : true,
            fetchBenefits  : true
        });
        TimerMixin.setTimeout(() => {
        this.fetchData()
            .then(() => {
            this.setState({ 
                refreshing     : false,
                fetchBenefits  : false,
                isLoading      : false
            })
        })
        .catch((err) => {
            console.log("Fallo", err)
        })
        }, 2500)
    }

    async fetchData() {
        this.setState({isLoading:true})
        return fetch('http://romedicalsistema.com/api/getBeneficios.php', {
            method: 'get',
        }).then((response) => response.json())
          .then((res) => {
            this.props.dispatch({ type: 'LOAD_BENEFITS', payload: res.data })
            this.setState({fetchBenefits: false});        
        }).catch((error) => {
                console.error(error);
                this.setState({fetchBenefits: true});            
        });
    }
    goLogIn = () => {
        NavigationService.navigate("LoginScreen");
        // this.props.screenProps.mainNavigation.navigate("LoginScreen");
    }
    getParameterByName(url,name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
const styles = StyleSheet.create({
    cardStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginTop: 5,
        marginBottom: 5
    },
    contDescrip: {
      height: 60,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: 22,
      paddingLeft: 22
    },
    leftContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    rightContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    title: {
      fontFamily: 'Gotham-Medium',
      marginBottom: 10,
      fontWeight: 'bold',
      color:'#65BBE9',
      fontSize: 23,
    },
    text: {
      fontFamily: 'Gotham-Light',
      textAlign: 'auto',
      fontSize: 21,
      padding:11
    }
  });

const mapStateToprops = state => {
    return{
        userIsLogged: state.userIsLogged,
        user: state.user,
        password: state.password,
        benefits: state.benefits
    }
}

export default connect(mapStateToprops)(ListBenefits);
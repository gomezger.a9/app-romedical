import React from 'react';
import { View, StyleSheet, Keyboard, Text, Image } from "react-native";
import { InputItem, Button } from '@ant-design/react-native';
import { BackHandler } from 'react-native'
import TimerMixin from 'react-timer-mixin';

class ForgetPasswordScreen extends React.Component{
    state = {
        email: "",
        message: "",
        errorEmail: false
    }
    constructor(props){
        super(props);
        navigation = this.props.navigation;
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    setMessage(message){
        this.setState({message})
        TimerMixin.setTimeout(
            () => { this.setState({message: ""}) },
            2000
        );
    }

    restoreEmail = () => {

        Keyboard.dismiss();

        if(!this.validateEmail(this.state.email)){
            this.setMessage("Formato de email inválido");
            this.setState({errorEmail:true});
        }else{
            this.setState({isLoading: true});
            fetch('http://romedicalsistema.com/api/forgetPassword.php', {
                method: 'post',
                body: 'email='+this.state.email,
                headers: { 'Content-type': 'application/x-www-form-urlencoded' }
            }).then((response) => response.json())
                .then((res) => {
                    console.log("REEEEES");
                    console.log(res);

                    this.setState({isLoading: false});
                    
                    if(res.status == 200){
                        this.setMessage("Se ha enviado un mail a "+this.state.email+" para cambiar la contraseña.",5000);
                        TimerMixin.setTimeout(
                            () => { navigation.navigate('LoginScreen'); },
                            5000
                        );
                    }else if(res.status == 400){
                        this.setMessage("El mail "+this.state.email+" no se encuentra registrado.",5000);
                    }
                }).catch((error) => {
                    console.error(error);
                    this.setState({isLoading: false});
                });
        } 

    }

    render(){
        return (
            <View style={styles.container}>
                <View style={{justifyContent: 'center',alignItems: 'center', marginBottom: 50}}>
                    <Image
                        style={{
                            width: 288,
                            height: 36,
                        }}
                        source={require('../assets/images/logoheader.png')}
                    />
                </View>
                <Text style={{marginBottom: 25, textAlign: "center", color: 'grey', fontWeight:'bold'}}>
                    Recupere su cuenta. Ingrese su e-mail.
                </Text>
                <InputItem
                    clear
                    error={this.state.errorEmail}
                    placeholder="Email"
                    style={{marginBottom: 15}}
                    // ref= {(el) => { state.email = el; }}
                    onChangeText={(el) => {this.state.email = el; this.setState({errorEmail: false})}}
                >
                </InputItem>
                <Button type="primary" style={{marginBottom: 5, backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.restoreEmail}>Recuperar</Button>
                <Text style={{marginTop: 5, textAlign: "center", color: 'grey'}}>{this.state.message}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 15
    }
});

export default (ForgetPasswordScreen);


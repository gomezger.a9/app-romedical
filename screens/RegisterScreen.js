import React from 'react';
import { View, StyleSheet, TextInput, Text, Image, Dimensions } from "react-native";
import { Button, InputItem } from '@ant-design/react-native';
import axios from 'axios';
import { connect } from 'react-redux';
import { BackHandler, ActivityIndicator, Keyboard } from 'react-native'
import TimerMixin from 'react-timer-mixin';

const screenWidth = Dimensions.get('window').width


class LoginScreen extends React.Component{
    _didFocusSubscription;
    _willBlurSubscription;

    state = {
        email: "",
        password: "",
        confirmPassword: "",
        message: "",
        isLoading: false,
        errorEmail: false,
        errorPass: false,
        showForgetPassButton: false
    }

    constructor(props){
        super(props);
        navigation = this.props.navigation;
        this.handleBackButton = this.handleBackButton.bind(this);
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
        BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton() {
        this.props.navigation.goBack(null);
        return true;
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={{justifyContent: 'center',alignItems: 'center', marginBottom: 50}}>
                    <Image
                        style={{
                            width: 288,
                            height: 36,
                        }}
                        source={require('../assets/images/logoheader.png')}
                    />
                </View>
                <InputItem
                    clear
                    type="email"
                    error={this.state.errorEmail}
                    style={{marginBottom: 15}}
                    placeholder="Ingrese su email"
                    // ref= {(el) =>  el = this.state.email }
                    onChangeText={(el) => {this.state.email = el; this.setState({errorEmail: false})}}
                />
                <InputItem
                    clear
                    type="password"
                    error={this.state.errorPass}
                    style={{marginBottom: 15}}
                    placeholder="Ingrese una contraseña"
                    // ref= {(el) => { el = this.state.password; }}
                    onChangeText={(el) => {this.state.password = el; this.setState({errorPass: false})}}
                />
                <InputItem
                    clear
                    type="password"
                    error={this.state.errorPass}
                    style={{marginBottom: 15}}
                    placeholder="Confirmar contraseña"
                    // ref= {(el) => { el = this.state.password; }}
                    onChangeText={(el) => {this.state.confirmPassword = el; this.setState({errorPass: false})}}
                />
                <Button type="primary" style={{marginBottom: 5, backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.registerUser}>Registrarse</Button>
                {this.state.showForgetPassButton ? <Button type="primary" style={{marginBottom: 5, backgroundColor: "#5cc0ee", borderColor: "#5cc0ee"}} onPress={this.goRestorePass}>Recuperar contraseña</Button> : <Text></Text>}
                <ActivityIndicator size="large" color="#5cc0ee" animating={ this.state.isLoading }/>
                <Text style={{marginTop: 5, textAlign: "center", color: 'grey'}}>{this.state.message}</Text>
            </View>
        );
    }

    onBackButtonPressAndroid = () => {
        return false;
    };

    setMessage(message, time=2000){
        this.setState({message});
        TimerMixin.setTimeout(
            () => { this.setState({message: ""}) },
            time
        );
    }
    registerUser = () => {
        navigation.navigate('RegisterScreen');
    }

    enterDisconnected = () => {
        navigation.navigate('MainScreen');
    }

    goRestorePass = () => {
        navigation.navigate('ForgetPasswordScreen');
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    registerUser = () => {
        Keyboard.dismiss();

        if(!this.validateEmail(this.state.email)){
            this.setMessage("Formato de email inválido");
            this.setState({errorEmail:true});
        }else if(this.state.password === ""){
            this.setMessage("Debe completar la contraseña");
            this.setState({errorPass:true});
        }else if(this.state.confirmPassword !== this.state.password){
            this.setMessage("Las contraseñas deben coincidir");
            this.setState({errorPass:true});
        }else{
            this.setState({isLoading: true});
            fetch('http://romedicalsistema.com/api/registerUser.php', {
                method: 'post',
                body: 'email='+this.state.email+'&password='+this.state.password,
                headers: { 'Content-type': 'application/x-www-form-urlencoded' }
            }).then((response) => response.json())
                .then((res) => {
                    console.log("REEEEEES");
                    console.log(res);
                    this.setState({isLoading: false});
                    if(res.status == 201){
                        // this.props.dispatch({type: 'LOG_IN', data: res.data})
                        // navigation.navigate('MainScreen');
                        this.setMessage("El mail "+this.state.email+" está pendiente de activación, se ha enviado un mail para activarlo.",5000);
                        TimerMixin.setTimeout(
                            () => { navigation.navigate('LoginScreen'); },
                            5000
                        );
                    }else if(res.status == 200){
                        this.setMessage("Se ha enviado un mail a "+this.state.email+" para activar la cuenta.",5000);
                        TimerMixin.setTimeout(
                            () => { navigation.navigate('LoginScreen'); },
                            5000
                        );
                    }else if (res.status == 408){
                        // this.setState({errorEmail:true, errorPass: true});
                        this.setState({showForgetPassButton: true});
                        this.setMessage("El mail "+this.state.email+" ya se encuentra registrado.");
                    }else if(res.status = 409){
                        this.setMessage("El mail "+this.state.email+" no se encuentra registrado, por favor acérquese a una sucursal.");
                    }
                }).catch((error) => {
                    console.error(error);
                    this.setState({isLoading: false});
                });
        }    
    }
}

function mapStateToProps(state){
    return{
        email: state.email,
        password: state.password
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 15,
    }
});

export default connect(mapStateToProps)(LoginScreen);



// initialState = {user: "lala1", password: "lala2"};
initialState = {selectedMainScreenTab: "home", screenToNavigate: "" };

export function appReducer(state = initialState, action) {
    
    switch(action.type){
        case 'LOAD_USER_DATA':
            return{
                ...state,
                clientId: action.data.clientId,
                email: action.data.mail,
                phone : action.data.phone,
                birthdate : action.data.birthdate,
                address: action.data.address,
                codigoPostal: action.data.codigoPostal,
                localidad: action.data.localidad,
                name: action.data.name,
                doc: action.data.doc,
                profesion: action.data.profesion,
                emergenciaMedicaTel: action.data.emergenciaMedicaTel,
                emergenciaMedica: action.data.emergenciaMedica,
                obraSocialNAfiliado: action.data.obraSocialNAfiliado,
                obraSocialPlan: action.data.obraSocialPlan,
                obraSocial: action.data.obraSocial,
                hijos: action.data.hijos,
                gender: action.data.gender,
                saldoTarjeta: action.data.saldoTarjeta,
            }
        break;
        case 'LOG_IN':
            return {
                ...state,
                userIsLogged: true,
            }    
        break;
        case 'LOG_OUT':
            return{
                ...state,
                email: "",
                password: "",
                name: "",
                userIsLogged: false
            }
        break;
        case 'BAD_PASSWORD':
            return {
                ...state,
                userIsLogged:false
            }
            break;
        case 'SET_TOKEN':
            return {
                ...state,
                token: action.payload
            }
        break;
        case 'LOAD_NEWS':
            return{
                ...state,
                news: action.payload
            }
        break;
        case 'LOAD_BENEFITS':
            return{
                ...state,
                benefits: action.payload
            }
        break;
        case 'LOAD_AFFECTIONS':
            return{
                ...state,
                affections: action.payload
            }
        break;
        case 'LOAD_TREATMENTS':
            return{
                ...state,
                treatments: action.payload
            }
        break;
        case 'LOAD_SHIFTS':
            return{
                ...state,
                shifts: action.payload
            }
        break;
        case 'CHANGE_MAINSCREEN_TAB':
            return{
                ...state,
                selectedMainScreenTab: action.payload
            }
        case 'CHANGE_GENDER':
            return{
                ...state,
                gender: action.payload
            }
        break;
        case 'NAVIGATE_TO_SCREEN':
            return{
                ...state,
                screenToNavigate: action.payload
            }
        break;
    }
    // console.log(state);
    return state;
}
import Expo from 'expo';
import { Asset, AppLoading, Font, Expo } from 'expo';
Expo.Asset;
Asset;
import React from 'react';
import LoginScreen from './screens/LoginScreen';
import MainScreen from './screens/MainScreen';
import SplashScreen from './screens/SplashScreen';
import ForgetPasswordScreen from './screens/ForgetPasswordScreen';
import RegisterScreen from './screens/RegisterScreen';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Provider } from "react-redux";
import { Provider as ProviderAD } from '@ant-design/react-native';
import { createStore } from 'redux';
import { appReducer } from './reducers/AppReducer';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { PersistGate } from 'redux-persist/integration/react';

// import { Dimensions} from 'react-native';
// import { CardStackStyleInterpolator } from 'react-navigation/src/views/CardStackStyleInterpolator'

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, appReducer)
const store = createStore(persistedReducer);
const persistor = persistStore(store);
// const store = createStore(appReducer);

export default class App extends React.Component {
  state = {
    theme: null,
    currentTheme: null,
    isReady: false,
  };
  changeTheme = (theme, currentTheme) => {
    this.setState({ theme, currentTheme });
  };
  async componentDidMount() {
    console.disableYellowBox = true;
    await Font.loadAsync(
      'antoutline',
      // eslint-disable-next-line
      require('@ant-design/icons-react-native/fonts/antoutline.ttf')
    );

    await Font.loadAsync(
      'antfill',
      // eslint-disable-next-line
      require('@ant-design/icons-react-native/fonts/antfill.ttf')
    );
    // eslint-disable-next-line
    this.setState({ isReady: true });
  }
  
  render() {
    const { theme, currentTheme, isReady } = this.state;
    if (!isReady) {
      return <AppLoading />;
    }

    return (
      <Provider store={store}>
        <ProviderAD theme={theme}>
            <PersistGate persistor={persistor}>
              <AppStackNavigator
                onNavigationStateChange={this.handleNavigationChange}
                screenProps={{ changeTheme: this.changeTheme, currentTheme }}>
              </AppStackNavigator>
            </PersistGate>
        </ProviderAD>
      </Provider>
    );
  }

  handleNavigationChange = () =>{

  }
}

const AppStackNavigator = createAppContainer(createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
  },
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
      gesturesEnabled: false,
      cardStack: {
        gesturesEnabled: false,
      },
    },
  },
  MainScreen: {
    screen: MainScreen,
    navigationOptions: {
      gesturesEnabled: false,
      cardStack: {
        // gesturesEnabled: false,
      },
    },
  },
  ForgetPasswordScreen: {
    screen: ForgetPasswordScreen,
    navigationOptions: {
      cardStack: {
        gesturesEnabled: false,
      },
    },
  },
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      cardStack: {
        gesturesEnabled: false,
      },
    },
  },
},
{cardStyle: { backgroundColor: '#FFFFFF' }}
));

